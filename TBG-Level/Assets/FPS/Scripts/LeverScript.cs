﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverScript : MonoBehaviour
{

    bool doorOpen = false;
    public GameObject door;

    // Start is called before the first frame update\
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (doorOpen == false)

            {
                doorOpen = true;
                // door.GetComponent<MeshCollider>().enabled=(false);
                //door.GetComponent<MeshRenderer>().enabled = (false);
                door.GetComponent<Animator>().Play("Door Open");
                Debug.Log("Door Opened");
            }

            else

            {
                doorOpen = false;
                door.GetComponent<Animator>().Play("Door Close");
                Debug.Log("Door Closed");
                //door.GetComponent<MeshCollider>().enabled = (true);
                //door.GetComponent<MeshRenderer>().enabled = (true);
            }

        }
    }
}

