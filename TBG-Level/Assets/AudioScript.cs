﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]

public class AudioScript : MonoBehaviour
{

    bool playing = false;
    public AudioSource audiosor;
    public AudioClip clip; 

    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (playing == false)
        {
            playing = true;
            audiosor.Play();
        }
    }
}